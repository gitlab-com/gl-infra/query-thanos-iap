PRODUCTION_PROJECT=gitlab-ops
FUNCTION_NAME=query-thanos-infra-kpi
RUNTIME=python37
REGION=us-central1

.PHONY: all
all: validate test deploy

.PHONY: validate
validate:
	pycodestyle --first main.py

.PHONY: test
test:
	python test_main.py

.PHONY: deploy
deploy:
	gcloud -q --project $(PRODUCTION_PROJECT) beta functions deploy $(FUNCTION_NAME) --region $(REGION) --env-vars-file .env.credentials.yaml --runtime $(RUNTIME) --trigger-http
