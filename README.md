## Query-Thanos-IAP

GCP (Google Cloud Platform) resources sitting behind IAP (Identity-Aware Proxy) requires certain authentication to be able to queried systematically. This small Python script makes it possible to systematically query our Thanos resource that sits behind IAP in GCP. 

### Developing and Deploying
```
# Clone the repository first
git clone query-thanos-iap

# Change to the cloned directory 
cd query-thanos-iap

# Initialize pipenv 
pipenv shell 

# Install all the dependencies 
pipenv install 

# Validate 
make validate

# Test 
make test

# Deploy 
make deploy
```

When working locally, dependencies defined in `Pipfile` are used for unittesting...etc. However, Google Function requires a `requirements.txt` to manage dependencies; thus, the `requirements.txt` is used only for deploying to GCF. 

### Configuration
The following environment variables are needed:
1. `GOOGLE_APPLICATION_CREDENTIALS` - This should point to the .json key associated with the service account. 
2. `CLIENT_ID` - This is the client ID of the IAP resource.
3. `THANOS_QUERY_RANGE_URL` - This is the IAP protected reosource URL 
4. Also, it needs GCP project name, function name and region provided in the `Makefile`. 