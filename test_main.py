import unittest
import os
import json
from main import query_thanos

class TestQueryThanos(unittest.TestCase):
    TEST_QUERY = "up{fqdn=\"bastion-01-inf-ops.c.gitlab-ops.internal\"}&start=2019-11-20T20:10:30.781Z&end=2019-11-20T20:10:30.782Z&step=15s"

    def test_query_thanos(self):
        response = json.loads(query_thanos(None, self.TEST_QUERY))
        self.assertEqual("success", response['status'])

if __name__ == '__main__':
    unittest.main()